# README

This repository contains the code utilised in processing my Master's thesis. All code in this repository is made available under the terms of the GNU GPL v3 (see included LICENSE.md).

The two script files utilised in analysis are as follows: 

## **beta.R**

This script contains all the code used to conduct the distance-decay analyses. It calculates distance matrices for all variables, investigates relationships between community similarity and geographic distance, creates Mantel correlograms and partitions the community similarity variance. For the morphological data, it calculates similarity matrices, and then fits separate MRMs for each environmental variable against morphological similarity. Functions of note include *partitioner2* which is the function that calculates variance from a set of distance matrices.

## **vegan.R**

This script conducts all the NMDS ordinations. It also contains a number of functions that plot vegan ordination objects using ggplot2.

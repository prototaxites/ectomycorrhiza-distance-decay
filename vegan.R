## Scratchpad

## Libraries
library(dplyr)
library(vegan)
library(stringr)
library(gridExtra)
library(ggplot2)
library(cowplot)

################### GLOBAL SETTINGS ##################################
remove_singletons <- TRUE

################### FUNCTIONS ########################################
plot_ordination <- function(object, environment, group, display = "sites", signif = 0.05, showlegend = TRUE, textsize = 4){
  ## This function plots out a vegan ordination of type DCA (decorana) or NMDS (metaMDS).
  ## Takes 4 arguments, a vegan object, an environmental matrix, the name of a
  ## grouping variable within that environmental matrix, and a cutoff value
  ## for statistical significance

  ## Rename the group column for simplicity
  names(environment)[names(environment) == group] <- 'group'
  ## Save the scores (first two axes) to a data frame and add the group variable
  scores <- as.data.frame(scores(object, display = display))[,1:2]
  scores <- cbind(scores, group = environment$group)
  scores$group <- str_to_title(scores$group)

  ## Begin building the GGPlot object now so that we can extract the axes ranges
  p <- ggplot(scores, aes(x = scores[,1], y = scores[,2], colour = factor(group))) +
    geom_point(size = 2.5) +
    coord_fixed() + #+ ## need aspect ratio of 1!
    xlab(names(scores)[1]) + ylab(names(scores)[2]) +
    scale_color_brewer(name = "Tree Species", palette = "Set2") +
    #theme_minimal() + theme(legend.position = "bottom", axis.line.x = element_line(size = 0.3, color = "#A3A3A3"), axis.line.y = element_line(size = 0.3, color = "#A3A3A3"))
    theme(legend.position = "bottom",
          axis.title = element_text(size = 20),
          axis.text = element_text(size = 15),
          legend.text = element_text(size = 15))

  if(showlegend == FALSE){
    p <- p + theme(legend.position = "none")
  }
  ## Create a "ggplot_build" object from which we can extract axis ranges
  q <- ggplot_build(p)

  ## Generate the environmental matrix
  env_matrix <- envfit(object, environment, permutations = 999, na.rm = TRUE)
  ## Pull out the scores and scale them using the multiplier
  env_scores <- as.data.frame(scores(env_matrix, display = "vectors"))

  ## Generate a multiplier using the axis ranges, in order to scale the biplot arrows
  ## such that they fit within the plot. Code taken from vegan package function
  ## ordiArrowMul, licensed under the GPL 2.
  ##
  ## https://github.com/vegandevs/vegan/blob/master/R/ordiArrowMul.R
  u <- c(q$panel$ranges[[1]]$x.range, q$panel$ranges[[1]]$y.range)
  u <- u - rep(c(0,0), each = 2)
  r <- c(range(env_scores[,1], na.rm = TRUE), range(env_scores[,2], na.rm = TRUE))
  rev <- sign(diff(u))[-2]
  if (rev[1] < 0)
    u[1:2] <- u[2:1]
  if (rev[2] < 0)
    u[3:4] <- u[4:3]
  u <- u/r
  u <- u[is.finite(u) & u > 0]
  arrow.mul <- 0.75 * min(u)

  ## Scale the biplot arrows, and then add a variable column so we can get variable
  ## names as text.
  env_scores <- env_scores * arrow.mul
  env_scores <- cbind(env_scores, variable = row.names(env_scores))

  ## Get the p values and only keep vectors with a p value smaller than the cutoff
  env_scores <- cbind(env_scores, prob = env_matrix$vectors$pvals)
  env_scores <- env_scores[env_scores$prob <= signif,]

  env_scores$variable <- str_replace_all(env_scores$variable, "_", " ")
  env_scores$variable <- str_replace_all(env_scores$variable, "\\.", " ")
  env_scores$variable <- str_replace_all(env_scores$variable, "np", "N:P ")
  env_scores$variable <- str_replace_all(env_scores$variable, "no", "NO ")
  env_scores$variable <- str_replace_all(env_scores$variable, "nh", "NH ")
  env_scores$variable <- str_to_title(env_scores$variable)

  ## Finish building the ggplot object
  p <- p + geom_segment(data = env_scores, aes(x = 0, xend = env_scores[,1], y = 0, yend = env_scores[,2]),
                        arrow = arrow(length = unit(0.25, "cm")), colour = "grey", show.legend = FALSE) +
    geom_text(data = env_scores, aes(x = env_scores[,1], y = env_scores[,2], label = variable, colour = NULL), size = textsize, show.legend = FALSE)

  return(p)
}

gg_ordisurf <- function(object, environment, variable, variable_title, group, display = "sites", binwidth, showlegend = TRUE){
  ## Rename the group column for simplicity
  names(environment)[names(environment) == group] <- 'group'
  ## Save the scores (first two axes) to a data frame and add the group variable
  scores <- as.data.frame(scores(object, display = display))[,1:2]
  environment <- cbind(scores, environment)

  pattern <- paste0("^", variable, "$")
  index <- grep(pattern, colnames(environment))

  ordi <- ordisurf(object ~ environment[,index])$grid
  ordi.full <- expand.grid(x = ordi$x, y = ordi$y) #get x and ys
  ordi.full$z <- as.vector(ordi$z) #unravel the matrix for the z scores
  ordi.full <- data.frame(na.omit(ordi.full))

  p <- ggplot(environment, aes(x = environment[,1], y = environment[,2])) +
    geom_point(size = 2.5, alpha = 0.8, aes(shape = group)) +
    coord_fixed() + #+ ## need aspect ratio of 1!
    #scale_colour_gradient(high = "darkgreen", low = "darkolivegreen1") +
    xlab(names(environment)[1]) + ylab(names(environment)[2]) #+
    #theme_bw()

  if(!missing(binwidth)){
    p <- p + geom_contour(data = ordi.full, aes(x = x, y = y, z = z, colour = rev(..level..)),  binwidth = binwidth)
  } else {
    p <- p + geom_contour(data = ordi.full, aes(x = x, y = y, z = z, colour = rev(..level..)))
  }

  if(showlegend == TRUE){
    #sets the name of the legend for shape, and says which symbols we want (equivalent to the pch command in plot)
    p <- p + scale_shape_manual("Tree Species",  values = c(1,16, 0, 15)) +
      guides(colour = guide_colourbar(title = variable_title, label.theme = element_text(angle = 90, size = 10))) +
      theme(legend.key = element_blank(),  #removes the box around each legend item
            legend.position = "bottom", #legend at the bottom
            legend.direction = "horizontal",
            legend.box = "horizontal",
            legend.box.just = "centre")
  } else {
    p <- p + scale_shape_manual("Tree Species",  values = c(1,16, 0, 15), guide = FALSE) +
      guides(colour = guide_colourbar(title = "", label.theme = element_text(angle = 0, size = 10), label.position = "left")) +
      ggtitle(variable_title) +
      theme(legend.key = element_blank(),  #removes the box around each legend item
            legend.position = "left", #legend at the bottom
            legend.direction = "vertical",
            legend.box = "vertical",
            legend.box.just = "centre")
  }

  print(p)
  return(p)
}

surf_all_signif <- function(object, environment){
  ## Creates a list of ordisurf ggplots for variables that are significant.

  ## This snippet uses envfit to determine significance
  fit <- envfit(object, environment, permutations = 999, na.rm = TRUE)
  rnames <- rownames(fit$vectors$arrows)
  pvals <- fit$vectors$pvals
  envs <- rnames[which(pvals < 0.05)]

  ## This snippet fits a gam() model to determine significance;
  ## it works but produces too many graphs as the method is not
  ## multivariate.
  # e2 <- environment[,!names(environment) == "tree_species"]
  # require(mgcv)
  # scrs <- data.frame(scores(object, display = "sites", scaling = 3))
  # envs <- vector()
  # for(i in names(e2)){
  #   dat <- with(e2, cbind(scrs, env = e2[,names(e2) == i]))
  #   mod <- gam(env ~ s(dat[,1], dat[,2], k = 10), data = dat)
  #   if(summary(mod)[[22]][4] <= 0.05){
  #     envs <- c(envs, i)
  #   }
  # }


  plots <- list()
  for(i in 1:length(envs)){
    varname <- str_replace_all(envs[i], "_", " ") %>% str_replace_all("\\.", " ") %>% str_to_title() %>%
      str_replace_all("Np", "N:P ") %>% str_replace_all("No", "NO") %>% str_replace_all("Nh4", "NH") %>% str_replace_all("Som", "SOM")
    plot <- gg_ordisurf(object, environment, variable = envs[i], variable_title = varname, group = "tree_species", showlegend = FALSE)
    plots[[i]] <- plot
  }

  return(plots)
}


################### READ IN DATA #####################################
otus <- read.csv("otus/Outputs/otus_final.csv", row.names = 1)
otus <- otus[!rownames(otus) == "BEECH_PLOT3",]
otus <- otus[!rownames(otus) == "SPRUCE_PLOT2",]
## remove singletons

environment <- read.csv("Environmental Data/Data/enironment_nometadata.csv", row.names = 1)
environment <- environment[!rownames(environment) == "BEECH_PLOT3",]
environment <- environment[!rownames(environment) == "SPRUCE_PLOT2",]
## Remove some of the environmental variables/metadata elements, including plot names, plot size, measurement years,
## needle mass data (might be useful to standardise foliage measurements if they are not already), meteorological data
## (too many NAs) and soil solution data (too many NAs).
environment <- select(environment, -ICPF.Nr, -plot_size, -contains("year"), -contains("mass"), -ph_origin, -dep_n_total, -contains("ss"))
environment$tree_species <- factor(environment$tree_species)

if(remove_singletons) {
  otus <- otus[, colSums(otus > 0) > 1]
}

morphology <- read.csv("Morphology/MORPHOLOGY.csv", row.names = 1)
morphology <- select(morphology, -shape)
m_environment <- environment[match(rownames(morphology), rownames(environment)),]

################### SPLIT OTUS BY SPECIES ############################
otus_pine <- as.matrix(otus[str_detect(rownames(otus), "^[P]"),])
otus_spruce <- as.matrix(otus[str_detect(rownames(otus), "^[S]"),])
otus_oak <- as.matrix(otus[str_detect(rownames(otus), "^[O]"),])
otus_beech <- as.matrix(otus[str_detect(rownames(otus), "^[B]"),])

################### SPLIT ENVIRONMENT BY SPECIES #####################
# match up the envt to the plots, getting the same order, so the rows
# of the matrices should match up.
#any(is.na(envt_to_veg))

envt_to_veg_pine <- match(rownames(otus_pine), rownames(environment))
envt_pine <- environment[envt_to_veg_pine,]

envt_to_veg_spruce <- match(rownames(otus_spruce), rownames(environment))
envt_spruce <- environment[envt_to_veg_spruce,]

envt_to_veg_oak <- match(rownames(otus_oak), rownames(environment))
envt_oak <- environment[envt_to_veg_oak,]

envt_to_veg_beech <- match(rownames(otus_beech), rownames(environment))
envt_beech <- environment[envt_to_veg_beech,]

################### ORDINATION #######################################
## all species
otus_dca <- decorana(otus)
otus_nmds <- metaMDS(otus, k = 2, trymax = 100)

full_dca_graph <- plot_ordination(otus_dca, environment, "tree_species")
full_nmds_graph <- plot_ordination(otus_nmds, environment, "tree_species")

ggsave("Jim-analysis/Results/Ordination/OTUS_nmds.pdf", full_nmds_graph, width = 11, height = 8)
ggsave("Jim-analysis/Results/Ordination/OTUS_nmds.svg", full_nmds_graph, width = 11, height = 8)

## Pine
pine_dca <- metaMDS(otus_pine, k = 2, trymax = 100)
pine_dca_graph <- plot_ordination(pine_dca, envt_pine, "tree_species", showlegend = FALSE) + ggtitle("Pine") +
  xlim(-1, 1.6) + ylim(-1, 1.6)

## Spruce
spruce_dca <- metaMDS(otus_spruce, k = 2, trymax = 100)
spruce_dca_graph <- plot_ordination(spruce_dca, envt_spruce, "tree_species", showlegend = FALSE) + ggtitle("Spruce") +
  xlim(-1, 1.6) + ylim(-1, 1.6)

## Oak
oak_dca <- metaMDS(otus_oak, k = 2, trymax = 100)
oak_dca_graph <- plot_ordination(oak_dca, envt_oak, "tree_species", showlegend = FALSE) + ggtitle("Oak") +
  xlim(-1, 1.6) + ylim(-1, 1.6)

## Beech
beech_dca <- metaMDS(otus_beech, k = 2, trymax = 100)
beech_dca_graph <- plot_ordination(beech_dca, envt_beech, "tree_species", showlegend = FALSE) + ggtitle("Beech") +
  xlim(-1, 1.6) + ylim(-1, 1.6)

multi_ord <- grid.arrange(pine_dca_graph, spruce_dca_graph, oak_dca_graph, beech_dca_graph, ncol = 4)

ggsave("Jim-analysis/Results/Ordination/nmds_by_tree_species.pdf", multi_ord, width = 12, height = 4)
ggsave("Jim-analysis/Results/Ordination/nmds_by_tree_species.svg", multi_ord, width = 12, height = 4)
ggsave("Writeup/Figures/Ordination/nmds_by_tree_species.pdf", multi_ord, width = 12, height = 4)
ggsave("Writeup/Figures/Ordination/nmds_by_tree_species.svg", multi_ord, width = 12, height = 4)

## Morphology
morph_nmds <- metaMDS(morphology, k = 2, trymax = 100)
moprh_nmds_plot <- plot_ordination(morph_nmds, environment = m_environment, group = "tree_species")

ggsave("Jim-analysis/Results/Ordination/morphology_nmds.pdf")
ggsave("Jim-analysis/Results/Ordination/morphology_nmds.svg")

morph_otus <- plot_grid(full_nmds_graph, moprh_nmds_plot, labels = "auto")
ggsave("Jim-analysis/Results/Ordination/morph_otus.pdf", morph_otus, width = 11, height = 4)
ggsave("Jim-analysis/Results/Ordination/morph_otus.svg", morph_otus, width = 11, height = 4)
ggsave("Writeup/Figures/Ordination/morph_otus.pdf", morph_otus, width = 11, height = 4)
ggsave("Writeup/Figures/Ordination/morph_otus.svg", morph_otus, width = 11, height = 4)
